let readySetGo = false;

document.addEventListener("DOMContentLoaded", function (event) {

    buildCardCar(
        "1",
        "Ecto I",
        "http://2.bp.blogspot.com/-18reaXBMuCQ/T6IRLP2ylnI/AAAAAAAAAVY/fjGDoAilecg/s1600/pic-7.jpg"
    );

    buildCardCar(
        "2",
        "DeLorean",
        "https://i.blogs.es/74f053/ted_2844-x3/1366_2000.jpg"
    );

    //Capturar customEvent lanzado por move: en detail.winner el string del ganador
    document.addEventListener('winner', function (event) {
        alert("¡El ganador es: " + event.detail.winner + "!")
    });

});

function startCompetition() {
    if (readySetGo) {
        window.scroll(0, 1000)
        const contestants = document.querySelectorAll('.script__contestant');
        contestants.forEach(function (contestant) {
            move(contestant, "XXX", "right", 0);
        })
    } else {
        alert('¡No están listos los participantes!');
    }
}